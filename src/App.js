import "./App.css";
import Body from "./LayoutComponents/Body";
import Footer from "./LayoutComponents/Footer";
import Header from "./LayoutComponents/Header";

function App() {
  return (
    <div className='App'>
      <Header />
      <Body />
      <Footer />
    </div>
  );
}

export default App;
